## Systemadministration
### Vorlesung

---
## Virtualisierung

---
## Agenda Virtualiserung

* Was ist Emulation
* Was ist Virtualisierung
* Unterscheidung Emulation / Virtualisierung
* Arten der Virtualisierung
* Systemvirtualisierung
* Software Virtualisierung
* Andwendungsvirtualisierung
* Speichervirtualisierung
* Netzwerk Virtualisierung

#### Today its all Virtual

---
## Was ist Emulation
#### Bei der Hardware Emulation wird für den Gast die gesamte Hardware simuliert und vom Emulator für das Host System übersetzt.

---
## Emulation
![Emulation](_images/emulator.png)

---
## Eigenschaften einer Emulation
* Hoher Resourcenverbrauch
* Emulation jeglicher Hardware für den Gast möglich (C64 / PowerPC / SPARC)
* Simulation von Hardware Fehlern möglich (Disk / Memory)

---
## Beispiele
* Bochs (http://bochs.sourceforge.net/)
* Qemu (https://www.qemu.org/)

---
## Links

* Linux http://bellard.org/jslinux/
* PCjs https://www.pcjs.org/

---
## Was ist Virtualisierung
#### Die Abstraktion von Hardware in mehrere voneinander unabhängige Systeme und die Zuteilung der vorhandenen Resourcen

---
## Virtualisierung
#### Beispiel Virtuelle Maschine

![Aufbau Hypervisor](_images/aufbau-virtualisierung.png)

---
## Eigenschaften einer Virtualisierung
* Vermittlung eines exklusiven Zugriffs gegenüber dem Gastsystems
* Isolierung des Hostsystems und den anderen Gastsystemen
* Zuteilung der Resourcen (CPU / MEMORY / Speicher / Netzwerk)

---
## Geschichte der Virtualisierung
* IBM Host Systeme (1970)
  * viel Leistung aber hohe Kosten
	* Partitionierung des Systems im „BIOS“ möglich
	* Lizenzierung der Partitionen
  * LPAR / z/VM / KVM (Aktuell)

---
## Geschichte der Virtualisierung
* chroot (1986)
* Jails
* UserModLinux
* OpenVZ (2003)
* Laufzeitumgebungen (JAVA / .NET / Dalvik / Android ART)
* Paravirtualisierung ( VMware )

---
## Geschichte der Virtualisierung
* Hardware Virtualisierung X86 Intel / AMD
* Hardware Virtualisierung ARM
* MicroVMs (2018)

---
## Vorteile von Virtualisierten Systemen
* Bessere Nutzung der Resourcen (CPU / MEMORY / Speicher / Netzwerk)
* Homogenisierte Umgebung für das Gast Betriebssystem beziehungsweise die Anwendung
* Eliminierung der Hardware Abhängigkeit
* Flexibilität im Betrieb ( zum Beispiel: Hardware Update / Migrationen)
* Moderne Virtualisierungen haben weniger als 5% Overhead
* Sicherheit (Achtung Meltdown / Spectre Patches)

---
## Systemvirtualisierung
* Container / Jails
* Systemvirtualisierung mittels Hypervisor
* MicroVMs

---
## Container / Jails
* genannt OS Virtualisierung
* Host und Gast teilen sich einen Kernel (Single Kernel Image)
* somit an den Hostkernel gebunden
* Hohe Ausführungsgeschwindigkeit
* Wenig Hauptspeicheroverhead

---
## Container / Jails
![OS Virtualisierung](_images/os-virtualisierung.png)

---
## Security
#### Mechanismen des Host Betriebssystems

* Linux Kernel (cgroups / Namespaces)
* SELinux
* AppArmor

---
## Beispiele
* libcontainer (docker)
* Linux Containers (LXC)
* BSD Jails
* Solaris Zones
* OpenVZ
* Linux VServers
* User Mod Linux (Linux on Linux)

---
## Systemvirtualisierung via Hypervisor
* genannt Virtuelle Maschine
* Plattform / OS Unabhängig
* Jede Virtuelle Maschine hat einen eigenen Kernel und Speicherbereich
* Steuerung über den Virtual Machine Monitor (VMM)
* Hoher Hauptspeicherverbrauch
* Beschleunigung durch Hardwareunterstützung (Intel-VT / AMD-V)

---
## Hypervisor

![Hypervisor Types](_images/hypervisor-typen.png)

---
## Aufgaben des VMM
* Regelt den Zugriff auf die Hardware der Verschiedenen Virtuellen Maschinen
* Übersetzt die Speicheradressen der VM in die der Hardware
* Emulation der Hardware die nicht durch einfaches Umschreiben Virtualisierbar ist
* Nur Ring-0 Prozesse haben vollen Hardware Zugriff
* Umschreiben der Ring-0 CPU Instruktionen
* Kontextwechsel CPU Zykulusintesiv
  * Software Paravirtualisierung (Gastanpassungen)
  * Hardwareunterstützung (Intel-VT / AMD-V) Vollvirtualisierung

---
## Ring 0 Problem

![cpu-ring0](_images/cpu-ring-0.png)

---
## Paravirtualisierung

![cpu-ring0-paravitualisation](_images/cpu-ring-0-paravirtualisation.png)

---
## Vollvirtualisierung

![cpu-ring0-fullvitualisation](_images/cpu-ring-0-fullvirtualisation.png)

---
#### Hardwareunterstütztevirtualisierung

![cpu-ring0-hwvitualisation](_images/cpu-ring-0-hwvirtualisation.png)

---
## Type 1 Hypervisor
* Virtual Machine Monitor
* Native Virtualisierung
* Läuft direkt auf der Hardware
* Kein Betriebssystem nötig
* Stellt den Gast OS direkt die Resourcen zur Verfügung

---
## Beispiele
* XEN (priviligierte VM über dem Hypervisor)
* Vmware ESXi
* Microsoft Hyper-V

---
## Type 2 Hypervisor
* VirtualMachineMonitor
* Läuft auf dem Betriebssystem
* OS stellt IO Resourcen zur Verfügung
* Stellt dem Gast OS direkt die Resourcen zur Verfügung

---
## Beispiele
* KVM
* Bhyve
* Virtualbox
* VMware Player / Workstation / Server

---
## Überblick Beispiele

![Überblick Hypervisor](_images/ueberblick-hypervisor.png)

---
## MicroVMs
##### Vor allem Cloud Provider benötigten mehr Sicherheit im Bereich der Container und Entwickelten daher einen Hybriden.

---
## MicroVMs (Skizze)
![MicroVM](_images/microvms.png)

---
## Eigenschaften von MicroVMs

* Leichtgewichtansatz vom Container
* zusätzliche Sicherheit durch den Einsatz einer VMM
* Startzeit zirka 100ms

---
## Beispiele
* Microsoft Native Containers
* Kata Container (https://katacontainers.io/)
* Amazon Firecracker (https://firecracker-microvm.github.io/)

---
## Nested Virtualisation
#### (Matroschka Prinzip)
* Bezeichnet die Möglichkeit ein bis n viele Virtuelle Maschinen in einer Virtuellen Maschine zu starten.
* Hardware Resourcenintensiv
* muss vom Hypervisor unterstützt werden
* Hardware Unterstützung

---
## Nested Virtualisation
#### (Skizze)

![Nested Virtualisation](_images/nested-virtualisation.png)

---
## Virtualisierung und Security
* Spectre und Meltdown (2017)
* Konzept MicroVMs
* Containersecurity (CVE on RUNC) (2019)

---
## Security

* Meltdown
* Spectre

##### Problem war eher in der Fehlerhaften Implementierung der CPU Hardware zu finden als in den VMM Implementierungen

---
## Anwendungs Virtualisierung
* Bezeichnet die Abschottung von Anwendungen / Prozessen
* Diese werden dann in einer Laufzeitumgebung vom Betriebssystem durch
eine Abstraktionsschicht getrennt ausgeführt
* Hinterläst keine Spuren am Wirtsystem

---
## Verwendung

* Malware Analyse
* Software Deployment (Application Streaming)
* Sicherheitsaspekt

---
## Anwendungs Virtualisierung
#### Beispiele:
* AppArmor
* Microsoft App-V
* Citrix XenAPP
* VMware Thinapp
* Sandbox(ie)

---
## Software Virtualisierung
#### Um in der Softwareentwicklung im Speziellen aber im Deployment und Betrieb flexibler und Sicherer zu werden hat Virtualisierung auch in der Softwareentwicklung einen Platz gefunden.

---
## Beispiele

* Just in Time Compiler
* Java VM (JVM)
* .NET Core Common Language Runtime (CLR)
* Dalvik / ART Android

---
## .NET Core

![Anwendungsvirtualisierung](_images/anwendungsvirtualisierung.png)

---
## Java

![Anwendungsvirtualisierung Java](_images/anwendungsvirtualisierung-java.png)

---
## API Virtualisierung
#### Bezeichnet die Übersetzung oder Abstraktion von API Calls einer Anwendung in die Zielplattform

* Applikationsentwicklung / Testing / Mokking
* Software Deployment / Betrieb (Betriebssystem Unabhänging)

#### Beispiele:
* Cygwin
* WINE

---
## Hardware Virtualisierung
#### Es werden ganze Systeme (Serverpatitionierung) oder aber auch nur Teile davon Virtualisiert (CPU)

* Systempartitionierung LogicalPartition (IBM-Mainframe)
* Hitachi Cumpute Blade System LPAR für X86
* CPU Virtualiserungserweiterungen
* Blade Systeme (Netzteil / Netzwerk / SAN)

---
## Speicher Virtualisierung
#### Es werden große Speicherbereiche aufgeteilt und mehreren Hosts angeboten

* LUNś
* VVLOLS
* Logical Volume Manager
* Moderne Dateisysteme (ZFS / BTRFS)
* Objectstores (Amazon S3)

---
## Netzwerk Virtualisierung
##### Es werden große Bandbreiten und Datenmengen aufgeteilt und mehreren Kunden (Tenants) zur Verfügung gestellt.

---
## Beispiele
* LWL Multipex Verfahren
	* DWDM
	* CWDM  
* Virtuelle Netzwerke
  * VLAN (L2)
  * VRF (L3)

---
## Beispiele
* Overlay Netzwerke
  * MPLS
  * VXLAN
* Tunnel Machanismen
	* GRE / L2TP/ VPN / PPTP
	* FCOE / NVMe over Fabrics

---
## Bedenke bei Virtualisierung
* Leistungsverlust (Vernachlässigbar auch bei Datenbanken)
* Nicht alles ist Virtualisierbar (zb. Lizenzdongels )
* Ausfall einer Hardware eventuell mehrere Dienste betroffen (200 VM)
* Virtualisierung ist Komplexe Software (paravirtualisierung)
* eventuell Sicherheit (Übernahme von Host/Gast Systemen)
  * Spectre
  * Meltdown
* Trennung Entwicklung / Test / Produktivumgebung
* Resourcen Einteilung / Limitierung

---
## Virtualisierung Next Step
#### Durch die Virtualisierung des Computers ist dieser von der Erstellung bis zum Löschen Steuerbar und eine Automatisierung über das Betriebssystem hinaus ist möglich.

* Schnittstelle fürs Monitoring der Virtuellen Maschinen
* Schnittstellen fürs Backup
* Schnittstelle für die Orchestrierung
* Schnittstelle zum Benutzer / Administrator

---
## Beispiele:
* VMware PowerCLI
* Libvirt (KVM , XEN , LCX , VMware , Hyper-V)

---
# Fragen ???

---
## Links
* Docker (Wikipedia) https://de.wikipedia.org/wiki/Docker_(Software)
* C64 Emulation im Browser (https://web.archive.org/web/20190820161014/http://www.winz-it.de/c64/emulator/index.html)
* Linux Emulation im Browser (http://bellard.org/jslinux/)
* Security Container RUNC CVE (https://nvd.nist.gov/vuln/detail/CVE-2019-5736)
* Security Container RUNC CVE Heise (https://www.heise.de/security/meldung/Luecke-in-Container-Runtime-runc-ermoeglicht-Host-Uebernahme-4306487.html)

---
# Fragen ???

---

## Nennen sie 4 Aufgaben des Virtal Machine Monitors (VMM) ?

---

## Skizzieren sie die Komponenten der Containervirtualisierung und nennen sie 3 Merkmale ?

---

## Skizzieren sie die Komponenten der Virtualisierung mit Hypervisor (Systemvirtualisierung) und nennen sie 3 Merkmale ?

---
## Skizzieren sie die Komponenten einer Virtualiserung mit MicroVMs und nennen sie 3 Merkmale ?

---

## Nennen Sie drei Merkmale der Systemvirtualisierung mit Hypervisor und Beschreiben Sie diese ?

---
## Nenne mindestens 3 Eigenschaften der Emulation ?

---

## Nennen sie 4 Merkmale der Anwendungsvirtualisierung ?

---
## Welche Sicherheitsmechanismen gibt es bei Containern oder Jails ?

---
## Beschreibe den Begriff Nasted Virtualisation und nenne 4 Merkmale
