# SAM Vorlesung Virtualisierung
Das Gitlab Repository ist die Quelle um die Vorlesung in verschiedene Formate zu erstellen.


Option 1 (Online):
  * Browse https://gitpitch.com/rstumpner/sam-vo-virtualiserung/master?grs=gitlab&t=simple

Option 2 (Online-PDF)
  * Über eine CI Pipeline wird mit Pandoc ein PDF zum Download erzeugt dieses kann in der Pipeline heruntergeladen werden.
  
Option 3 (Lokal):
  * git clone https://gitlab.com/rstumpner/sam-vo-virtualiserung
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

Wünsche und Anregungen können gerne in den Gitlab Issues angemerkt werden.
